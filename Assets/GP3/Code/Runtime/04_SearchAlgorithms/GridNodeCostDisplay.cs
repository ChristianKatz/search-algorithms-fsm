﻿using TMPro;
using UnityEngine;

namespace GP3._04_SearchAlgorithms
{
    public class GridNodeCostDisplay : MonoBehaviour
    {
        // grid node script of the node
        private GridNode _gridNode;
        // display of the cost
        private TextMeshProUGUI _costDisplay;
        void Start()
        {
            // get the grid node
            _gridNode = GetComponentInParent<GridNode>();
            // get the cost display of the node
            _costDisplay = GetComponent<TextMeshProUGUI>();
        }
        
        void Update()
        {
            // update the cost of the node when the A* algorithm is calculating there
            _costDisplay.text = _gridNode.TotalGridNodeCost().ToString();
        }
    }
}
