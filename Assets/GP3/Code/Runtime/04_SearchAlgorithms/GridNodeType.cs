﻿namespace GP3._04_SearchAlgorithms
{
	public enum GridNodeType
	{
		Ground,
		Wall,
		Water,
		// added the type to be able to use it in the grid node script
		Mountain,
	}
}