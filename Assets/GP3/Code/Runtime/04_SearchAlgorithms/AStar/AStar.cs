﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GP3._04_SearchAlgorithms.AStar
{
    public class AStar : SearchBase
    {
        protected override void InitializeSearch()
        {
            _startNode = _startMarker.ClosestGridNode;
            _goalNode = _endMarker.ClosestGridNode;

            foreach (GridNode gridNode in _visited.Keys)
            {
                gridNode.Reset();
            }
			
            _openList = new List<GridNode>();
            _visited = new Dictionary<GridNode, GridNode>();

            _openList.Add(_startNode);
        
            _startNode.CostSoFar = 0;
        }
        
        // NOTE: I copied the Dijkstra and added the heuristic at the specific lines
        protected override bool StepToGoal()
        {
            // sort all 
            // add the heuristic cost to get the total cost
            _openList = _openList.OrderBy(n => n.CostSoFar + n.Heuristic).ToList();
            GridNode current = _openList[0];
			
            // goal found
            if (current == _goalNode)
            {
                return true;
            }

            foreach (GridNode next in current.Neighbours)
            {
                if (next.IsWall)
                {
                    continue;
                }
                
                float newCost = current.CostSoFar + next.Cost;
                bool alreadyVisited = _visited.ContainsKey(next);
                if (alreadyVisited)
                {
                    if (newCost < next.CostSoFar)
                    {
                        next.CostSoFar = newCost;
                        // add the heuristic cost to calculate the remaining distance to the goal
                        next.Heuristic = GetHeuristic(_goalNode, next);
                        _visited[next] = current;
                        _openList.Add(next);
                        next.SetGridNodeSearchState(GridNodeSearchState.Queue);
                    }
                }
                else
                {
                    _openList.Add(next);
                    _visited.Add(next, current);

                    next.CostSoFar = newCost;
                    // add the heuristic cost to calculate the remaining distance to the goal
                    next.Heuristic = GetHeuristic(_goalNode, next);
                    
                    next.SetGridNodeSearchState(GridNodeSearchState.Queue);
                }
            }

            _openList.Remove(current);
            current.SetGridNodeSearchState(GridNodeSearchState.Processed);
            // not yet finished
            return false;
        }
        
        private float GetHeuristic(GridNode goal, GridNode next)
        {
            return (goal.transform.position - next.transform.position).magnitude;
        }
    }
}

