﻿using UnityEngine;
using UnityEngine.UIElements;

namespace UEGP3.FSM.Quests
{
	public interface IQuestState
	{
		IQuestState Execute(QuestNPC npc);
		void Enter(QuestNPC npc);
		void Exit(QuestNPC npc);
	}
	
	public class QuestAvailableState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			if (Input.GetMouseButtonDown(0) && npc.IsPlayerClose)
			{
				return QuestNPC.QuestActiveState;
			}
			
			return QuestNPC.QuestAvailableState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.DisplayQuestIcon(npc.AvailableStateMesh);
			npc.SetNPCAnswer("Hey you! Go get that item for me, would ya?");
		}

		public void Exit(QuestNPC npc)
		{
			npc.DisplayQuestIcon(null);
			npc.SetNPCAnswer("");
		}
	}
	
	public class QuestActiveState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			// the time is running when the player accepts the quest
			// when the time is 0 the player failed to fulfill the quest
			if (npc.QuestTime() <= 0)
			{
				return QuestNPC.QuestFailState;
			}
			
			if (npc.RequirementsMet)
			{
				return QuestNPC.QuestTaskDoneState;
			}
			
			return QuestNPC.QuestActiveState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.QuestItem.Activate();
			npc.SetNPCAnswer("Remember to bring me my item please");
		}

		public void Exit(QuestNPC npc)
		{
			npc.SetNPCAnswer("");
		}
	}

	/// <summary>
	/// when the player doesn't collect the item in the given time, the quest is failed
	/// </summary>
	public class QuestFailState : IQuestState
	{
		/// <summary>
		/// update permanently the fail state, so that the quest can't be repeated
		/// </summary>
		/// <param name="npc"></param>
		/// <returns></returns>
		public IQuestState Execute(QuestNPC npc)
		{
			return QuestNPC.QuestFailState;
		}

		/// <summary>
		/// when the state is entered the following message is displayed to inform the player that he has lost
		/// </summary>
		/// <param name="npc"></param>
		public void Enter(QuestNPC npc)
		{
			npc.SetNPCAnswer("Damn you needed too long, you are a failure!");
		}

		/// <summary>
		/// nothing will be added because the quest failed and won't exit this state
		/// </summary>
		/// <param name="npc"></param>
		public void Exit(QuestNPC npc)
		{

		}
	}
	
	public class QuestTaskDoneState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			if (npc.IsPlayerClose && Input.GetMouseButtonDown(0))
			{
				return QuestNPC.QuestDoneState;
			}

			return QuestNPC.QuestTaskDoneState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.DisplayQuestIcon(npc.QuestTaskDoneStateMesh);
			npc.SetNPCAnswer("Ay what a great finding! Myyyy precious");
		}

		public void Exit(QuestNPC npc)
		{
			npc.DisplayQuestIcon(null);
			npc.SetNPCAnswer("");
		}
	}
	
	public class QuestDoneState : IQuestState
	{
		public IQuestState Execute(QuestNPC npc)
		{
			return QuestNPC.QuestDoneState;
		}

		public void Enter(QuestNPC npc)
		{
			npc.SetNPCAnswer("My precious..");
		}

		public void Exit(QuestNPC npc)
		{
		}
	}
}